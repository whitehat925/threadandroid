package com.example.handlerthread;


import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.NonNull;

public class ExampleHandlerThread extends HandlerThread {
    private static final String TAG = "ExampleHandlerThread";
    private Handler handler;
    public static final  int EXAMPLE_TASK = 1;

    public ExampleHandlerThread() {
        super("ExampleHandlerThread", Process.THREAD_PRIORITY_BACKGROUND);

    }

    @Override
    protected void onLooperPrepared() {
        handler = new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                switch (msg.what){
                    case EXAMPLE_TASK:
                        Log.d(TAG, "Example Task, arg1:"+msg.arg1 + ", obj:" +msg.obj);
                        for (int i = 0; i < 4; i++) {
                            Log.d(TAG, "handleMessage:" + i);
                            SystemClock.sleep(1000);
                        }
                        break;
                }
            }
        };
    }

    public Handler getHandler(){
        return handler;
    }
}
