package com.example.handlerthread;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;

import static com.example.handlerthread.ExampleHandlerThread.EXAMPLE_TASK;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private ExampleHandlerThread handlerThread = new ExampleHandlerThread();
    private Handler threadHandler;
    private ExampleRunable1 exampleRunable1 = new ExampleRunable1();
    private Object token = new Object();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handlerThread.start();
    }

    public void doWork(View view) {
//        threadHandler.postDelayed(new ExampleRunable1(),2000);
//        threadHandler.post(new ExampleRunable2());
//        handlerThread.getHandler().post(new ExampleRunable1());
//        handlerThread.getHandler().post(new ExampleRunable1());
//        handlerThread.getHandler().postAtFrontOfQueue(new ExampleRunable2());
        Message message = Message.obtain(handlerThread.getHandler());
        message.what = EXAMPLE_TASK;
        message.arg1 = 23;
        message.obj = "Obj String";
        message.sendToTarget();
//        handlerThread.getHandler().sendMessage(message);

//        handlerThread.getHandler().sendEmptyMessage(1);
        handlerThread.getHandler().postAtTime(exampleRunable1, token, SystemClock.uptimeMillis());
        handlerThread.getHandler().post(exampleRunable1);
    }
    public void removeMessages(View view) {
//        handlerThread.getHandler().removeCallbacksAndMessages(null);
//        handlerThread.getHandler().removeCallbacks(exampleRunable1);
//        handlerThread.getHandler().removeMessages(1);
        handlerThread.getHandler().removeCallbacks(exampleRunable1, token);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handlerThread.quit();
    }

    static class ExampleRunable1 implements Runnable{

        @Override
        public void run() {
            for (int i = 0; i < 4; i++) {
                Log.d(TAG, "Runnable1:"+i);
                SystemClock.sleep(1000);
            }
        }
    }

    static class ExampleRunable2 implements Runnable{

        @Override
        public void run() {
            for (int i = 0; i < 4; i++) {
                Log.d(TAG, "Runnable2:" +i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}