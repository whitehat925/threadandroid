package com.example.anr;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;


import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.tv_ip)
    TextView tvIP;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        new AsyncTaskNetwork(this, tvIP).execute("https://www.youtube.com/watch?v=P1nEAfmYels&list=PL33lvabfss1wDeQMvegg_OZQfaXcbqOQh&index=14");
    }
}