package com.example.loopermessagequeuehandlerexplained;


import android.os.Bundle;
import android.os.Message;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import static com.example.loopermessagequeuehandlerexplained.ExampleHandler.TASK_A;
import static com.example.loopermessagequeuehandlerexplained.ExampleHandler.TASK_B;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "MainActivity";
    private ExampleLooperThread exampleLooperThread = new ExampleLooperThread();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startThread(View view) {
        exampleLooperThread.start();
    }
    public void stopThread(View view) {
        exampleLooperThread.looper.quit();
    }
    public void taskA(View view) {
//            Handler handler = new Handler(exampleLooperThread.looper);
//            handler.post(new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 0; i < 5; i++){
//                    Log.d(TAG, "run:" + i);
//                    SystemClock.sleep(1000);
//                }
//            }
//        });
        Message msg = Message.obtain();
        msg.what = TASK_A;
        exampleLooperThread.handler.sendMessage(msg);
    }
    public void taskB(View view) {
        Message msg = Message.obtain();
        msg.what = TASK_B;
        exampleLooperThread.handler.sendMessage(msg);
    }
}