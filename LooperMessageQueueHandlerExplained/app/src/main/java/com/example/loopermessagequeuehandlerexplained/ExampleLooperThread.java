package com.example.loopermessagequeuehandlerexplained;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

public class ExampleLooperThread extends Thread{

    private static String TAG = "ExampleLooperThread";
    public Handler handler;
    public Looper looper;
    @Override
    public void run() {
        super.run();
        Looper.prepare();
        looper = Looper.myLooper();
        handler = new ExampleHandler();
        Looper.loop();
//        for (int i = 0; i < 5; i++){
//            Log.d(TAG, "run:" +i);
//            SystemClock.sleep(1000);
//        }
//
        Log.d("TAG", "End of run()");
    }
}
