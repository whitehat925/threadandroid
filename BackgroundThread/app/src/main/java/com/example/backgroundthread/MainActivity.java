package com.example.backgroundthread;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private Button buttonStartThread;
    private Handler mainHandler = new Handler();
    private volatile boolean stopThread = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonStartThread = findViewById(R.id.button_start_thread);
    }

    public void startThread(View view){
//        ExampleThread exampleThread = new ExampleThread(10);
//        exampleThread.start();
        stopThread = false;
        ExampleRunnable exampleRunnable = new ExampleRunnable(10);
        new Thread(exampleRunnable).start();
    }

    public void stopThread(View view){
        stopThread = true;
    }

    class ExampleThread extends Thread{
        int seconds;

        public ExampleThread(int seconds) {
            this.seconds = seconds;
        }

        @Override
        public void run() {
            super.run();
            for (int i = 0; i < 10; i++){
                Log.d(TAG, "StartThread:" + i);
                try {
                    Thread.sleep(1000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
    }

    class ExampleRunnable implements Runnable{

        int seconds;

        public ExampleRunnable(int seconds) {
            this.seconds = seconds;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10; i++){
                Log.d(TAG, "StartThread:" + i);
                if (stopThread)
                    return;
                if (i == 5){
                    //c1
//                    mainHandler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            buttonStartThread.setText("50%");
//                        }
//                    });
                    //c2
//                    buttonStartThread.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            buttonStartThread.setText("50%");
//                        }
//                    });
                    //c3
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            buttonStartThread.setText("50%");
                        }
                    });
                }
                try {
                    Thread.sleep(1000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
